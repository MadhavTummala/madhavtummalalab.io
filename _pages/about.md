---
permalink: /
title: "About me"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

I graduated from Indian Institute of Technology, Bhubaneswar with an Bachelors degree in Computer Science in 2020. I currently work as an analyst at Goldman Sachs, working on distributed computing of big data. 

While in college, I did research internships in areas of SDN (P4) and severless computing and worked on various projects in Robotics and full stack developement. For my bachelors thesis project, I worked on optimising data sharing protocols for wireless sensor networks with [Dr.Sudipta Saha](https://www.iitbbs.ac.in/profile-print.php?furl=sudipta).

In my free time, I like to automate tasks, watch movies, swim and explore new technology. I am also actively learning Spanish and Go. A more informal website can be found [here](https://madhavchoudhary.github.io).