---
title: "Concurrent Transmission Based Data Sharing with Run-Time Variation of TDMA Schedule"
collection: publications
permalink: /publication/minicastR
excerpt: 'We implement MiniCastR, an efficient many-to-many communication protocol designed for low power wireless sensor networks'
date: 2020-11-18
venue: 'The 45th IEEE Conference on Local Computer Networks (LCN), 2020'
citation: 'Madhav Tummala, Sudipta Saha, "Concurrent Transmission Based Data Sharing with Run-Time Variation of TDMA Schedule", In proceedings of IEEE Conference on Local Computer Networks (LCN), 2020, Sydney, Australia, 2020'
---
We implement MiniCastR, an efficient many-to-many communication protocol designed for low power wireless sensor networks

<!-- [Read paper here](https://www.usenix.org/conference/atc18/presentation/oakes) -->
