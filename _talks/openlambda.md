---
title: "Concurrent Transmission Based Data Sharing with Run-Time Variation of TDMA Schedule"
collection: talks
type: "Talk"
permalink: /talks/openlambda
venue: "Virtual Conference, IEEE LCN"
date: 2020-11-18
location: "Sydney, Australia"
---

Gave a conference presentation in virtual mode.
<!-- [See the slides](https://edoakes.github.io/files/openlambda_slides.pdf). -->
